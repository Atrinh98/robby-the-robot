using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeneticAlgorithm;

namespace GeneticAlgorithmTest
{

    [TestClass]
    public class GeneticAlgorithmClassTest{

        int populationSize = 200;
        int numberOfGenes = 243;
        int lengthOfGene = 7;
        double mutationRate = 1;
        double eliteRate = 1;
        int numberOfTrials = 1;
        FitnessEventHandler FitnessEvent;
        
        int potentialSeed = 1;


        // For testing the Fitness Function and not having the FitnessEventHandler null.
        private double FitnessFunction(IChromosome chromosome, IGeneration generation){
            return 100;
        }

        [TestMethod]
        public void GenerateGenerationInternalTest(){
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  eliteRate, numberOfTrials, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;

            Assert.AreEqual(1, Generation.CurrentGenerationNumber);
            geneticAlgorithm.GenerateGeneration();
            Assert.AreEqual(2, Generation.CurrentGenerationNumber);
        }

        [TestMethod]
        public void ConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  eliteRate, numberOfTrials, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;

            Assert.AreEqual(populationSize, geneticAlgorithm.PopulationSize);
            Assert.AreEqual(numberOfGenes, geneticAlgorithm.NumberOfGenes);
            Assert.AreEqual(lengthOfGene, geneticAlgorithm.LengthOfGene);
            Assert.AreEqual(mutationRate, geneticAlgorithm.MutationRate);
            Assert.AreEqual(eliteRate, geneticAlgorithm.EliteRate);
            Assert.AreEqual(numberOfTrials, geneticAlgorithm.NumberOfTrials);
            Assert.AreEqual(FitnessEvent, geneticAlgorithm.FitnessCalculation);

            Generation gen = geneticAlgorithm.CurrentGeneration as Generation;

            int[] expectedGenes = {1, 0, 3, 5, 4, 3, 2, 6, 0, 4, 0, 1, 2, 6, 4, 
            4, 1, 4, 4, 4, 6, 0, 1, 2, 5, 1, 5, 2, 5, 6, 3, 5, 4, 0, 6, 5, 6, 0, 
            3, 3, 1, 6, 4, 2, 3, 4, 4, 3, 1, 2, 3, 1, 2, 4, 2, 1, 6, 0, 6, 3, 4, 
            0, 0, 1, 0, 2, 3, 2, 6, 6, 5, 1, 0, 1, 3, 6, 2, 5, 6, 0, 3, 2, 0, 3, 
            2, 2, 2, 0, 4, 5, 5, 5, 0, 0, 6, 1, 6, 3, 2, 0, 0, 2, 3, 2, 3, 2, 0, 
            1, 1, 3, 5, 1, 0, 5, 6, 2, 6, 4, 5, 4, 6, 3, 4, 1, 0, 0, 0, 0, 2, 1, 
            0, 6, 3, 5, 6, 0, 5, 4, 6, 0, 2, 1, 5, 1, 0, 4, 4, 1, 0, 3, 1, 1, 4, 4, 
            1, 4, 4, 2, 2, 3, 1, 6, 6, 0, 2, 5, 4, 2, 5, 6, 3, 2, 5, 5, 2, 5, 5, 3, 
            0, 2, 2, 6, 0, 5, 6, 6, 2, 6, 4, 2, 3, 3, 2, 2, 6, 2, 2, 4, 5, 1, 6, 2, 2, 
            1, 0, 6, 3, 5, 1, 3, 5, 1, 1, 0, 1, 1, 6, 1, 0, 3, 3, 5, 4, 3, 3, 6, 6, 2, 
            6, 0, 2, 0, 4, 5, 2, 0, 3, 6, 4, 0, 2, 3, 4};

            CollectionAssert.AreEqual(expectedGenes, gen.Population[11].Genes);
        } 

        [TestMethod]
        public void GenerateGenerationTest(){
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  eliteRate, numberOfTrials, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;

            Generation nextGen = geneticAlgorithm.GenerateGeneration() as Generation;

            Assert.AreEqual(geneticAlgorithm.CurrentGeneration, nextGen);
            Assert.AreEqual(geneticAlgorithm.PopulationSize, nextGen.NumberOfChromosomes);
            Assert.AreEqual(2, Generation.CurrentGenerationNumber);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PopulationZeroConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(0, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  eliteRate, numberOfTrials, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PopulationNegativeConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(-1, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  eliteRate, numberOfTrials, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NumberOfGenesZeroConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            0, lengthOfGene, 
            mutationRate,  eliteRate, numberOfTrials, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NumberOfGenesNegativeConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
           -4, lengthOfGene, 
            mutationRate,  eliteRate, numberOfTrials, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NumberofTrialsZeroConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  eliteRate, 0, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NumberofTrialsNegativeConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  eliteRate, -10, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void EliteRateZeroConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  0, numberOfTrials, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void EliteRateNegativeConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  -100, numberOfTrials, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void MutationRateZeroConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            0,  eliteRate, numberOfTrials, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void MutationRateNegativeConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            -199,  eliteRate, numberOfTrials, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void FitnessFunctionNullConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  eliteRate, numberOfTrials, FitnessEvent, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;
        }
    }
}