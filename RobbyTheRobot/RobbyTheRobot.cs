﻿using System;
using GeneticAlgorithm;
using System.IO;

namespace RobbyTheRobot
{
    public class RobbyTheRobot : IRobbyTheRobot
    {   
        //Fields
        private  GeneticAlgorithm.GeneticAlgorithm _geneticAlgorithm;
        private Random _random;
        private readonly int?  _potentialSeed;
        private readonly int _numberOfGenes = 243;
        private readonly int _allele = 7;

        /// <summary>
        /// Represents Robby the Robot
        /// </summary> 
        public int NumberOfActions {get;}
        public int NumberOfTestGrids {get;}
        public int GridSize {get;}
        public int NumberOfGenerations {get;}
        public double MutationRate {get;}
        public double EliteRate {get;}

        /// <summary>
        /// An event raised when a file is written to disk
        /// </summary>
        public event FileEventHandler FileWritten;
        public FitnessEventHandler FitnessFunction;

        /// <summary>
        /// Constructor to create RobbyTheRobot object.
        /// </summary>
        public RobbyTheRobot(int numberOfGenerations, int populationSize, int numberOfTrials, int? potentialSeed,
        int gridSize = 10, int numberOfActions = 200, int numberOfTestGrids = 5, 
        double mutationRate = 0.01, double eliteRate = 5){

            if (numberOfTrials <= 0 || numberOfGenerations <= 0 || populationSize <=0 )
            throw new ArgumentException("Number of Trials, Population Size, Number of Generations cannot be 0 or lower");

            NumberOfGenerations = numberOfGenerations;
            GridSize = gridSize;
            NumberOfActions = numberOfActions;
            NumberOfTestGrids = numberOfTestGrids;
            MutationRate = mutationRate;
            EliteRate = eliteRate;
            
            if (potentialSeed != null){
                _random = new Random((int) potentialSeed);
                _potentialSeed = potentialSeed;
            }
            else{
                _random = new Random();
                _potentialSeed = potentialSeed;
            } 

            _geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, _numberOfGenes, _allele, MutationRate, 
            EliteRate, numberOfTrials, FitnessFunction += ComputeFitness, _potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;

            FileWritten += FileWrittenNotification;        
        }

        /// <summary>
        /// Used to generate a single test grid filled with cans in random locations. Half of 
        /// the grid (rounded down) will be filled with cans. Use the GridSize to determine the size of the grid
        /// </summary>
        /// <returns>Rectangular array of Contents filled with 50% Cans, and 50% Empty </returns>
        public ContentsOfGrid[,] GenerateRandomTestGrid(){
            Random random;
            if (_potentialSeed != null) random = new Random((int) _potentialSeed);
            else random = new Random();

            ContentsOfGrid[,] grid = new ContentsOfGrid[this.GridSize, this.GridSize];
            
            // Fill up all the grid with empty.
            for (int i = 0; i < this.GridSize; i++){
                for (int j = 0; j < this.GridSize; j++){
                    grid[i,j] = ContentsOfGrid.Empty;
                }
            }

            int canCount = 50;
            // Fill the grid with cans until it reaches 50.
            while (canCount > 0){
                int x = random.Next(GridSize);
                int y = random.Next(GridSize);
                if (CanOrEmpty(random) == ContentsOfGrid.Can && grid[x,y] == ContentsOfGrid.Empty){
                    grid[x, y] = ContentsOfGrid.Can;
                    canCount--;
                }
            }
            return grid;
        }


        /// <summary>
        /// Randomly places robby on a grid where the position does not have a can (For monogame)
        /// </summary>
        /// <param name="grid">The grid where we have the information of empty and cans</param>
        /// <returns>An array with the x and y position of Robby </returns>
        public int[] PlaceRobby(ContentsOfGrid[,] grid){
            bool placed = false;
            int[] coordinate = new int[2];
            while (!placed){
                int x = _random.Next(this.GridSize);
                int y = _random.Next(this.GridSize);
                if (grid[x, y] == ContentsOfGrid.Empty){
                    coordinate[0] = x;
                    coordinate[1] = y;
                    placed = true;
                }
            }
            return coordinate;
        }
        
        /// <summary>
        /// Randomly places robby on a grid where the position does not have a can (For parallels)
        /// </summary>
        /// <param name="grid">The grid where we have the information of empty and cans</param>
        /// <param name="random"> Random object </param>
        /// <returns>An array with the x and y position of Robby </returns>
        public int[] PlaceRobby(ContentsOfGrid[,] grid, Random random){
            bool placed = false;
            int[] coordinate = new int[2];
            while (!placed){
                int x = random.Next(this.GridSize);
                int y = random.Next(this.GridSize);
                if (grid[x, y] == ContentsOfGrid.Empty){
                    coordinate[0] = x;
                    coordinate[1] = y;
                    placed = true;
                }
            }
            return coordinate;
        }

        /// <summary>
        /// Randomly returns a can or empty
        /// </summary>
        /// <returns> Either a can or empty (ContentOfGrid) </returns>
        private ContentsOfGrid CanOrEmpty(Random random){
            if (random.Next(2) == 1) return ContentsOfGrid.Can;
            else return ContentsOfGrid.Empty; 
        }

        /// <summary>
        /// Moves robby and scores him based on the list of possible moves (Since the class is not public, made a method for monogame)
        /// </summary>
        /// <param name="moves">The list of moves</param>
        /// <param name="grid">The test grid to move robby on</param>
        /// <param name="rng">A random number generator</param>
        /// <param name="x">A start x position that is updated to his subsequent position</param>
        /// <param name="y">A start y position that is updated to his subsequent position</param>
        /// <returns>The score</returns>
        public double MoveRobbyVisualizer(Chromosome chromosome, ContentsOfGrid[,] grid, ref int x, ref int y){
             return RobbyHelper.ScoreForAllele(chromosome.Genes, grid, _random, ref x, ref y);
        }

        /// <summary>
        /// Evaluate the fitness of a population then we sort it
        /// If the generation is the same as 1, 20, 100, 200, 500, 1000, save to the folder the information
        /// Update the generation
        /// Repeat until the generation is the same as NumberOfGenerations
        /// </summary>
        /// <param name="folderPath">The path to where our folder is located to save our information </param>
        public void RobbyConsoleGetData(string folderPath){
            while(Generation.CurrentGenerationNumber <=  NumberOfGenerations){
                Generation currentGen = _geneticAlgorithm.CurrentGeneration as Generation;

                currentGen.EvaluateFitnessOfPopulation();
                currentGen.SortPopulation();

                if (Generation.CurrentGenerationNumber == 1 ||Generation.CurrentGenerationNumber == 20 || 
                Generation.CurrentGenerationNumber == 100 || Generation.CurrentGenerationNumber == 200 ||
                Generation.CurrentGenerationNumber == 500 || Generation.CurrentGenerationNumber == 1000)
                GeneratePossibleSolutions(folderPath);

                UpdateGeneration();
            }
            
        }

        //The library requires a ComputeFitness function that is called to determine the score. 
        //Note, your ComputeFitness function is responsible for generating a random grid and running 
        //robby through the grid and scoring his moves. The RobbyHelper class will assist you in the scoring part.
        /// <summary>
        /// The fitness function we pass to the delegate to get the score of the chromosome fitness.
        /// </summary>
        /// <param name="chromosome">The chromosome used to move robby depending on its genes </param>
        /// <param name="generation"> Just here for the delegate parameters (to match its signature)</param>
        /// <returns>The score for that chromosome</returns>
        private double ComputeFitness(IChromosome chromosome, IGeneration generation){
            Random random;
            if (_potentialSeed != null) random = new Random((int) _potentialSeed);
            else random = new Random();
            
            // Generate a grid with random values
            ContentsOfGrid[,] grid = GenerateRandomTestGrid();
           
            // Places robby randomly.
            int[] coordinates = PlaceRobby(grid, random);
            int x = coordinates[0];
            int y = coordinates[1];

            // Convert the parameters to their object counter part.
            Chromosome tempChromosome = chromosome as Chromosome;

            // Begin the score process by calling ScoreForAllele
            double fitness = 0;
            for (int i = 0; i < NumberOfActions; i++){
                fitness += RobbyHelper.ScoreForAllele(chromosome.Genes, grid, random, ref x, ref y);
            }

            return fitness;
        }

        /// <summary>
        /// Update to next generation.
        /// </summary>
        public void UpdateGeneration(){
            _geneticAlgorithm.GenerateGeneration();
        }
 
        /// <summary>
        /// Generates a series of possible solutions based on the generations and saves them to disk.
        /// The text files generated must contain a comma seperated list of the max score, 
        ///number of moves to display in the gui and all the actions robby will take (i.e the genes in the Chromosome).
        /// The top candidate of the 1st, 20th, 100, 200, 500 and 1000th generation will be saved.
        /// </summary>
        /// <param name="folderPath">The path of the folder where the text files will be saved</param>
        public void GeneratePossibleSolutions(string folderPath){
            // Path to the file
            string file = Generation.CurrentGenerationNumber.ToString() + ".txt";
            folderPath = @""+folderPath+"/"+file;

            // If the file does not exist, create it and append text.
            if(!File.Exists(folderPath)){
                using(StreamWriter sw = File.CreateText(folderPath)){
                    SaveData(sw);
                }
            }
            // If the file exist then just append to it.
            else{
                using(StreamWriter sw = File.AppendText(folderPath)){
                    SaveData(sw);
                }
            }
            
            // Event fired to notify user that a file has been used.
            FileWritten();
        }

        /// <summary>
        /// Generates a series of possible solutions based on the generations and saves them to disk.
        /// The text files generated must contain a comma seperated list of the max score, 
        ///number of moves to display in the gui and all the actions robby will take (i.e the genes in the Chromosome).
        /// The top candidate of the 1st, 20th, 100, 200, 500 and 1000th generation will be saved.
        /// </summary>
        /// <param name="sw">The file where we store our information about the run </param>
        private void SaveData(StreamWriter sw){
            sw.WriteLine("Generation, Max Score, Number of Moves, Genes");
            string details = "";
            Generation tempGen = _geneticAlgorithm.CurrentGeneration as Generation;
            details += Generation.CurrentGenerationNumber + ",";
            details += tempGen.MaxFitness + ",";
            details += NumberOfActions + ",";
            Chromosome bestGene = tempGen.Population[0];
            string population = "";
            for (int i = 0; i < bestGene.Genes.Length; i++){
                population += bestGene[i];
            }
            details += population;
            sw.WriteLine(details);
        }

        /// <summary>
        /// The function in the event that will be called to alert the user that a file has been created with the data.
        /// </summary>
        private void FileWrittenNotification(){
            string message = "Generation#" + Generation.CurrentGenerationNumber + " and best gene from it have been written to file.";
            Console.WriteLine(message);   
        }
    }
}
