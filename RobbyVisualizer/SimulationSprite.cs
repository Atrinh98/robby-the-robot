﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RobbyTheRobot;


namespace RobbyVisualizer
{
    public class SimulationSprite : DrawableGameComponent
    {
        // Fields
        private SpriteBatch _spriteBatch;
        private int _gridSize;
        private ContentsOfGrid[,] _grid;
        private Texture2D _tile;
        private Texture2D _can;
        private RobbyVisualizerGame _game;

        /// <summary>
        /// Constructor for the SimulationSprite object.
        /// </summary>
        /// <param name="game"> The base game </param>
        /// <param name="gridSize"> The height or base of the grid </param>
        /// <param name="grid"> The grid containing the information about empty and cans </param>
        public SimulationSprite(RobbyVisualizerGame game, int gridSize, ContentsOfGrid[,] grid) : base(game)
        {
            _gridSize = gridSize;
            _grid = grid;
            _game = game;
        }

        /// <summary>
        /// Initialize function (default)
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Load all the sprites needed for this drawable game content
        /// </summary>
        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _tile = _game.Content.Load<Texture2D>("Tiles");
            _can = _game.Content.Load<Texture2D>("Can");
        }

        /// <summary>
        /// To update the current grid to the param one
        /// </summary>
        /// <param name="grid"> The grid containing the empty or cans </param>
        public void UpdateGrid(ContentsOfGrid[,] grid){
            _grid = grid;
        }

        /// <summary>
        /// Game loop (default)
        /// </summary>
        /// <param name="gametime"> </param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// Draws the tiles and cans
        /// </summary>
        /// <param name="gametime"> </param>
        public override void Draw(GameTime gameTime)
        {
             _spriteBatch.Begin();
            int cans = 0;
            for (int i = 0; i < _gridSize; i++){
                for (int j = 0; j < _gridSize; j++){
                    //Tiles
                    _spriteBatch.Draw(_tile, new Rectangle(i * 32, j * 32, 32, 32), Color.White);
                    // Cans
                    if (_grid[i,j] == ContentsOfGrid.Can){
                        _spriteBatch.Draw(_can, new Rectangle(i * 32, j * 32, 32, 32), Color.White);
                        cans++;
                    }
                }
            }
            RobbyVisualizerGame.Cans = cans;

            _spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
