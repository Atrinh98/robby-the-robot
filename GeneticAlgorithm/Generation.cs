﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;

namespace GeneticAlgorithm
{
    public class Generation: IGenerationDetails
    {
        /// <summary>
        /// To keep track of the current Generation number. 
        /// </summary>
        public static int CurrentGenerationNumber {get; set;}
        
        // Fields
        private IGeneticAlgorithm _geneticAlgorithm;
        private FitnessEventHandler _fitnessEvent;
        public Chromosome[] Population {get; private set;}
        private int? _potentialSeed;

        /// <summary>
        /// Constructor for Generation where we also populate the population with random chromosome
        /// </summary>
        public Generation(IGeneticAlgorithm geneticAlgorithm, FitnessEventHandler fitnessEvent, int? potentialSeed){
            _geneticAlgorithm = geneticAlgorithm;

            Debug.Assert(fitnessEvent.GetInvocationList().Length > 0);

            _fitnessEvent += fitnessEvent;

            if (potentialSeed != null) _potentialSeed = (int) potentialSeed;
            else _potentialSeed = potentialSeed;
            Population = new Chromosome[_geneticAlgorithm.PopulationSize];
            PopulateFirstPopulation();
            CurrentGenerationNumber++;
        }

        /// <summary>
        /// Copy constructor for Generation where we also populate the population with elite parents
        /// </summary>
        public Generation(Generation copyGeneration){
            _potentialSeed = copyGeneration._potentialSeed;
            _geneticAlgorithm = copyGeneration._geneticAlgorithm;
            _fitnessEvent += copyGeneration._fitnessEvent;
            Population = new Chromosome[copyGeneration._geneticAlgorithm.PopulationSize];
            Array.Copy(copyGeneration.Population, Population, Population.Length);
            PopulateNextPopulation();
            CurrentGenerationNumber++;
        }
        
        /// <summary>
        /// The average fitness across all Chromosomes
        /// </summary>
        public double AverageFitness { get {
            double totalFitness = 0;
            foreach (Chromosome c in Population){
                totalFitness += c.Fitness;
            }
            return totalFitness / NumberOfChromosomes;
        } }

        /// <summary>
        /// The maximum fitness across all Chromosomes
        /// </summary>
        public double MaxFitness { get{
            double maxFitness = 0;
            foreach (Chromosome c in Population){
                if (c.Fitness > maxFitness) maxFitness = c.Fitness;
            }
            return maxFitness;
        }}

        /// <summary>
        /// Returns the number of Chromosomes in the generation
        /// </summary>
        public long NumberOfChromosomes { get { return Population.Length; } }

        /// <summary>
        /// Retrieves the IChromosome from the generation
        /// </summary>
        /// <value>The selected IChromosome</value>
        public IChromosome this[int index] { get {
            if(index < 0 || index > this.NumberOfChromosomes) throw new ArgumentOutOfRangeException();
            else return Population[index];
        }
        set{ Population[index] = value as Chromosome;} }

        /// <summary>
        /// Populate the population array for the first generation by using random chromosome.
        /// </summary>
        private void PopulateFirstPopulation(){
            for (int i = 0; i < Population.Length; i++){
                Population[i] = new Chromosome(_geneticAlgorithm.NumberOfGenes, _geneticAlgorithm.LengthOfGene, _potentialSeed);
            }
        }

        /// <summary>
        /// Populate the population array for the next generation by using elite parents.
        /// </summary>
        private void PopulateNextPopulation(){
            Chromosome[] nextPopulation = new Chromosome[Population.Length];

            for (int i = 0; i < Population.Length; i += 2){
                   
                Chromosome parent1 = SelectParent() as Chromosome;
                Chromosome parent2 = SelectParent() as Chromosome;

                Chromosome[] childs = parent1.Reproduce(parent2, _geneticAlgorithm.MutationRate) as Chromosome[];

                nextPopulation[i] = childs[0];
                nextPopulation[i+1] = childs[1];
            }

            Array.Copy(nextPopulation, Population, Population.Length);
        }

        /// <summary>
        /// Randomly selects a parent by comparing its fitness to others in the population
        /// </summary>
        /// <returns></returns>
        public IChromosome SelectParent(){
            Random random;
            if (_potentialSeed != null) random = new Random( (int) _potentialSeed);
            else random = new Random();
            
            // Get the elite chromosome into the elites array.
            Chromosome[] elites = Population.Take(ElitesNumber()).ToArray();
            // Choose randomly from one of the elites.            
            return elites[random.Next(elites.Length)];
        }

        /// <summary>
        /// Returns the position to skip all the non elites in the sorting Population.
        /// </summary>
        /// <return> Return the index where the elite is .</return>
        private int ElitesNumber(){
            decimal elitesNB = (decimal) (_geneticAlgorithm.PopulationSize * _geneticAlgorithm.EliteRate) / 100;
            elitesNB = Decimal.Ceiling(elitesNB);
            return (int) elitesNB;
        }

        /// <summary>
        /// Sort the Chromosome from highest to lowest.
        /// </summary>
        public void SortPopulation(){
            Array.Sort(Population);
            Array.Reverse(Population);
        }

        /// <summary>
        /// Computes the fitness of all the Chromosomes in the generation. 
        /// Note, a FitnessEventHandler deleagte is invoked for every fitness function that must be calculated and is provided by the user
        /// Note, if NumberOfTrials is greater than 1 in IGeneticAlgorithm, 
        /// the average of the number of trials is used to compute the final fitness of the Chromosome.
        /// </summary>
        public void EvaluateFitnessOfPopulation(){

            Parallel.ForEach(Population, chromosome => {
                double[] fitness = new double[_geneticAlgorithm.NumberOfTrials];
                Parallel.For(0, _geneticAlgorithm.NumberOfTrials, (i) => {
                    fitness[i] = _fitnessEvent(chromosome, this);
                });
                chromosome.Fitness = fitness.Average();
            });
        }
    }
}
