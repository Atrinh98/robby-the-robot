﻿using System;
using GeneticAlgorithm;
using RobbyTheRobot;
using System.Linq;
using System.IO;
using System.Diagnostics;

namespace RobbyIterationGenerator
{
    class ConsoleProgram
    {   
        // Static Fields
        private static RobbyTheRobot.RobbyTheRobot _robby;
        private static string _folderPath;
        
        /// <summary>
        /// The console application to run the simulation
        /// </summary>
        /// <param name="args"> </param>
        static void Main(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();
            _robby = CreateRobby();
            CreateSaveFolder();
            stopwatch.Start();
            Console.WriteLine("Calculation in Proress.");
            _robby.RobbyConsoleGetData(_folderPath);
            stopwatch.Stop();
            TimeSpan ts = stopwatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            ts.Hours, ts.Minutes, ts.Seconds,
            ts.Milliseconds / 10);
            Console.WriteLine("RunTime " + elapsedTime);
            Console.WriteLine("Robby has learned alot :)");
        }

        /// <summary>
        /// Ask the user where to save the files.
        /// If the folder already exist the just notify the user
        /// Else create the folder 
        /// Save the folderpath to the static field 
        /// </summary>
        private static void CreateSaveFolder(){
            string folder = "Data";
            _folderPath = "../"+folder;
            string folderPath = @"../"+folder;

            if (!Directory.Exists(folderPath)){
                Directory.CreateDirectory(folderPath);
                Console.WriteLine("Folder has been created."); 
            }
            else Console.WriteLine("Folder already exist.");
        }


        /// <summary>
        /// Creates a RobbyTheRobot object with user input as parameters
        /// </summary>
        /// <returns> RobbyTheRobot object with user input as parameters </returns>
        private static RobbyTheRobot.RobbyTheRobot CreateRobby(){
            Console.WriteLine("Welcome to Robby CLI");

            string generationMessage = "How many generations? Nothing for 1000.";
            int numberOfGenerations = CheckIntInput(generationMessage, 1000);

            string populationMessage = "What is the population size? Nothing for 100.";
            int populationSize = CheckIntInput(populationMessage, 100);
    
            string trialsMessage = "How many trials? Nothing for 100.";
            int numberOfTrials = CheckIntInput(trialsMessage, 100);

            string seedMessage = "Enter Potential seed? Nothing for null.";
            int? potentialSeed = CheckIntNullInput(seedMessage);

            return Robby.CreateRobby(numberOfGenerations, populationSize, numberOfTrials, potentialSeed) as RobbyTheRobot.RobbyTheRobot;
        }
        
        /// <summary>
        /// Shows the question and wait for the user to answer
        /// Takes the answer and do some data validation
        /// If it is good then we return the answer
        /// </summary>
        /// <param name="message">The message to display to the user what to type</param>
        /// <returns> the answer to the question </returns>
        private static int CheckIntInput(string message, int defaultValue){
            bool correct = false;
            string input = "";
            while (!correct){
                Console.WriteLine(message);
                input = Console.ReadLine();
                if (input == "") return defaultValue;
                int count = 0;
                foreach (char digit in input){
                    if(Char.IsDigit(digit) && digit != ' '){
                        count++;
                    }
                }
                if (count == input.Length) correct = true;
                
            }
            return Int32.Parse(input);
        }

        /// <summary>
        /// Shows the question and wait for the user to answer
        /// Takes the answer and do some data validation
        /// If it is good then we return the answer
        /// </summary>
        /// <param name="message">The message to display to the user what to type</param>
        /// <returns> the answer to the question </returns>
        private static int? CheckIntNullInput(string message){
            bool correct = false;
            string input = "";
            while (!correct){
                Console.WriteLine(message);
                input = Console.ReadLine();
                if (input == "") return null;
                if ("null".Equals(input, StringComparison.OrdinalIgnoreCase)) return null;

                int count = 0;
                foreach (char digit in input){
                    if(Char.IsDigit(digit)){
                        count++;
                    }
                }
                if (count == input.Length) correct = true;     
            }
            int? answer =  Int32.Parse(input);
            return answer;
        }

    }
}
