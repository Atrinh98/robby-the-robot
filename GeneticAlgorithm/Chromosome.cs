﻿using System;
using System.Diagnostics;

namespace GeneticAlgorithm
{
    public class Chromosome : IChromosome
    {
        // Fields
        private readonly int? _potentialSeed;

        /// <summary>
        /// Constructor for the Chromosome class.
        /// </summary>
        public Chromosome(int NbOfGenes, int length, int? potentialSeed){

            Debug.Assert(NbOfGenes > 0);
            
            Length = length;
            Genes = new int[NbOfGenes];

            if (potentialSeed != null) _potentialSeed = (int) potentialSeed;
            else _potentialSeed = potentialSeed;
            
            Random random;
            if (_potentialSeed != null) random = new Random( (int) _potentialSeed);
            else random = new Random();

            // Generates the Genes randomly
            for (int i = 0; i < NbOfGenes; i++){
                Genes[i] = random.Next(length);
            }
        }

        /// <summary>
        /// Copy Constructor for the Chromosome class.
        /// </summary>
        public Chromosome(Chromosome copyChromosome){
            _potentialSeed = copyChromosome._potentialSeed;
            Length = copyChromosome.Length;
            Genes = new int[copyChromosome.Genes.Length];
            Array.Copy(copyChromosome.Genes, this.Genes, copyChromosome.Genes.Length);
        }

        /// <summary>
        /// Constructor for the Chromosome class, use for file reading (Monogame part).
        /// </summary>
        public Chromosome(int[] genes, int length = 7, int? potentialSeed = null){
            Length = length;
            if (potentialSeed != null) _potentialSeed = potentialSeed;
            else _potentialSeed = potentialSeed;
            Genes = new int[genes.Length];
            Array.Copy(genes, Genes, genes.Length);   
        }
         
        /// <summary>
        /// The fitness score of the IChromosome
        /// </summary>
        /// <value>A value representing the fitness of the IChromosome</value>
        public double Fitness {get; set;}

        /// <summary>
        /// The Genes (entire int array) for the IChromosome
        /// </summary>
        /// <value>An array representing the Genes of the IChromosome</value>
        public int[] Genes { get;}

        /// <summary>
        /// Uses a crossover function to create two offspring, then iterates through the
        /// two child Chromosomes genes, changing them to random values according to the mutation rate.
        /// </summary>
        /// <param name="spouse">The Chromosome to reproduce with</param>
        /// <param name="mutationProb">The rate of mutation</param>
        /// <returns></returns>
        public IChromosome[] Reproduce (IChromosome spouse, double mutationProb){
            Random random;
            if (_potentialSeed != null) random = new Random((int) _potentialSeed);
            else random = new Random();

            Chromosome child1 = new Chromosome(this);
            Chromosome child2 = new Chromosome(spouse as Chromosome);


            // Chosen randomly
            int pointA = random.Next((int) this.Length);
            int pointB = random.Next( pointA, (int) this.Length);
            
            //Crossover
            int[] temp = new int[child1.Genes.Length];
            for (int i = 0; i < temp.Length; i++){
                temp[i] = child1.Genes[i];
            }

            for (int i = pointA; i < pointB; i++){
                child1.Genes[i] = child2.Genes[i];
                child2.Genes[i] = temp[i];
            }

            MutateChild(child1, mutationProb, random);
            MutateChild(child2, mutationProb, random);

            return new Chromosome[] {child1, child2};
        }

        /// <summary>
        /// Mutation to the allele when the percentage is right
        /// </summary>
        /// <param name="child">The Chromosome to mutate</param>
        /// <param name="rate">The rate of mutation</param>
        /// <param name="random"> Random object for rng</param>
        private void MutateChild(Chromosome child, double rate, Random random){
            for (int i = 0; i < child.Genes.Length; i++){
                if (random.NextDouble() <= rate){
                    child.Genes[i] = random.Next((int) Length);
                }
            }
        }

        /// <summary>
        /// Returns the current gene at the provided position
        /// </summary>
        /// <value></value>
        public int this[int index] {get { return Genes[index]; }}

        /// <summary>
        /// The length of the genes
        /// </summary>
        public long Length {get;}


        /// <summary>
        /// Compares this chromosome's fitness with another and returns and int depending on the result
        /// </summary>
        /// <param name="chromosomeCompare">The Chromosome to compare with</param>
        /// <returns></returns>
        public int CompareTo(IChromosome chromosomeCompare){
            if (chromosomeCompare == null){
                return 1;
            }

            //Bigger than
            if (Fitness > chromosomeCompare.Fitness){
                return 1;
            }
            // Equal
            else if (Fitness == chromosomeCompare.Fitness){
                return 0;
            }
            // Less than
            else{
                return -1;
            }
        }

    }
}
