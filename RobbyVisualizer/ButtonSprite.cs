using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RobbyTheRobot;


namespace RobbyVisualizer
{
    public class ButtonSprite : DrawableGameComponent
    {
        // Fields
        private SpriteBatch _spriteBatch;
        private Texture2D _oneButton;
        private Texture2D _twentyButton;
        private Texture2D _hundredButton;
        private Texture2D _twoHundredButton;
        private Texture2D _fiveHundredButton;
        private Texture2D _thousandButton;
        private Texture2D _background;
        private SpriteFont _arial;
        private RobbyVisualizerGame _game;
        private Rectangle[] _buttonRectangles;
        public  static bool Click {get; set;}
        private string _path;

        /// <summary>
        /// Constructor for ButtonSprite
        /// </summary>
        /// <param name="game"The base game</param>
        public ButtonSprite(RobbyVisualizerGame game) : base(game)
        {
            _game = game;
            Click = false;
            _path = "../Data/";

        }

        /// <summary>
        /// Initialize the rectangle needed to store the button
        /// </summary>
        public override void Initialize()
        {
            _buttonRectangles = new Rectangle[6];
            int row = 20;
            int col = 40;
            for (int i = 0; i < _buttonRectangles.Length; i++){
                if (i == 0 || i == 3)
                _buttonRectangles[i] = new Rectangle(row, col, 60, 80);
                if (i == 1 || i == 4)
                _buttonRectangles[i] = new Rectangle(row + 55, col, 60, 80);
                if (i == 2 || i == 5)    
                _buttonRectangles[i] = new Rectangle(row + 110, col, 60, 80);
                
                // Increment
                row += 50;
                // Reset
                if (i == 2){
                    row = 20;
                    col += 180;
                }
            }
            base.Initialize();
        }

        /// <summary>
        /// Load all the sprites needed for this drawable game content
        /// </summary>
        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _oneButton = _game.Content.Load<Texture2D>("1");
            _twentyButton = _game.Content.Load<Texture2D>("20");
            _hundredButton = _game.Content.Load<Texture2D>("100");
            _twoHundredButton = _game.Content.Load<Texture2D>("200");
            _fiveHundredButton = _game.Content.Load<Texture2D>("500");
            _thousandButton = _game.Content.Load<Texture2D>("1000");
            _background = _game.Content.Load<Texture2D>("Background");
            _arial = _game.Content.Load<SpriteFont>("Arial");
        }

        /// <summary>
        /// Wait for user to click on a button to then set data to main game
        /// </summary>
        /// <param name="gametime"> </param>
        public override void Update(GameTime gameTime)
        {
            if (!Click){
                MouseState mouse = Mouse.GetState();
                if(_buttonRectangles[0].Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed){
                    SetData("1.txt");
                }
                else if(_buttonRectangles[1].Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed){
                    SetData("20.txt");
                }
                else if(_buttonRectangles[2].Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed){
                    SetData("100.txt");
                }
                else if(_buttonRectangles[3].Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed){
                    SetData("200.txt");
                }
                else if(_buttonRectangles[4].Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed){
                    SetData("500.txt");
                }
                else if(_buttonRectangles[5].Contains(mouse.Position) && mouse.LeftButton == ButtonState.Pressed){
                    SetData("1000.txt");;
                }
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// Set the data to the main game when we click on any button to get the data of that button
        /// </summary>
        /// <param name="fileName">The location to where the file is located </param>
        private void SetData(string fileName){
            Click = true;
            string path = @""+_path+fileName;
            FileReader fileReader = new FileReader(path);
            RobbyVisualizerGame.Chromosome = fileReader.Chromosome;
            RobbyVisualizerGame.Generation = fileReader.Generation;
            RobbyVisualizerGame.Moves = fileReader.Moves;
            RobbyVisualizerGame.MovesLeft = RobbyVisualizerGame.Moves;
        }

        /// <summary>
        /// Draw the background and buttons + tells the user to click on a button to select data.
        /// </summary>
        /// <param name="gametime"> </param>
        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();
            if (!Click){
                //background to contain all the button 
                _spriteBatch.Draw(_background, new Vector2(0,0), Color.White);
                _spriteBatch.DrawString(_arial, "Click on a Button to choose a Generation",new Vector2(0,10), Color.White);
                // button list
                _spriteBatch.Draw(_oneButton, _buttonRectangles[0], Color.White);
                _spriteBatch.Draw(_twentyButton, _buttonRectangles[1], Color.White);
                _spriteBatch.Draw(_hundredButton, _buttonRectangles[2], Color.White);
                _spriteBatch.Draw(_twoHundredButton, _buttonRectangles[3], Color.White);
                _spriteBatch.Draw(_fiveHundredButton, _buttonRectangles[4], Color.White);
                _spriteBatch.Draw(_thousandButton, _buttonRectangles[5], Color.White);
            }
            _spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
