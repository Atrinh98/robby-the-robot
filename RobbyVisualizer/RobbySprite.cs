﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RobbyTheRobot;


namespace RobbyVisualizer
{
    public class RobbySprite : DrawableGameComponent
    {
        // Fields
        private SpriteBatch _spriteBatch;
        private Texture2D _robbySprite;
        private RobbyVisualizerGame _game;

        /// <summary>
        /// Constructor for the RobbySprite object.
        /// </summary>
        /// <param name="game"> The base game </param>
        public RobbySprite(RobbyVisualizerGame game) : base(game)
        {
            _game = game;
        }

        /// <summary>
        /// Initialize function (default)
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Load all the sprites needed for this drawable game content
        /// </summary>
        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _robbySprite = _game.Content.Load<Texture2D>("Robby");
        }

        /// <summary>
        /// Game loop (default)
        /// </summary>
        /// <param name="gametime"> </param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// Draws robby depending on its x and y values
        /// </summary>
        /// <param name="gametime"> </param>
        public override void Draw(GameTime gameTime)
        {
            _spriteBatch.Begin();
            _spriteBatch.Draw(_robbySprite, new Rectangle(RobbyVisualizerGame.GetX() * 32, RobbyVisualizerGame.GetY() * 32, 32, 32), Color.White);
            _spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
