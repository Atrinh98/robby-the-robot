using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobbyTheRobot;
using GeneticAlgorithm;

namespace RobbyTheRobotTest
{
    [TestClass]
    public class RobbyTheRobotClassTest
    {
        int numberOfGenerations = 1000;
        int populationSize = 200;
        int numberOfTrials = 1;
        int potentialSeed = 1;

        [TestMethod]
        public void UpdateGenerationTest()
        {
            RobbyTheRobot.RobbyTheRobot rob = Robby.CreateRobby(numberOfGenerations, populationSize, 
            numberOfTrials, potentialSeed) as RobbyTheRobot.RobbyTheRobot;

            Assert.AreEqual(1, Generation.CurrentGenerationNumber);
            rob.UpdateGeneration();
            Assert.AreEqual(2, Generation.CurrentGenerationNumber);
            rob.UpdateGeneration();
            Assert.AreEqual(3, Generation.CurrentGenerationNumber);
        }


        [TestMethod]
        public void GenerateRandomTestGridTest()
        {
            RobbyTheRobot.RobbyTheRobot rob = new RobbyTheRobot.RobbyTheRobot(numberOfGenerations, populationSize, 
            numberOfTrials, potentialSeed, 3);

            ContentsOfGrid[,] grid = rob.GenerateRandomTestGrid();
            ContentsOfGrid[,] expectedGrid = {{ContentsOfGrid.Can, ContentsOfGrid.Empty, ContentsOfGrid.Empty}, 
            {ContentsOfGrid.Can, ContentsOfGrid.Can, ContentsOfGrid.Can}, 
            {ContentsOfGrid.Can, ContentsOfGrid.Can, ContentsOfGrid.Can}};

            CollectionAssert.AreEqual(expectedGrid, grid);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NumberOfGenerationsNegativeConstructorTest()
        {
            RobbyTheRobot.RobbyTheRobot rob = Robby.CreateRobby(-100, populationSize, 
            numberOfTrials, potentialSeed) as RobbyTheRobot.RobbyTheRobot;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NumberOfGenerationsZeroConstructorTest()
        {
            RobbyTheRobot.RobbyTheRobot rob = Robby.CreateRobby(0, populationSize, 
            numberOfTrials, potentialSeed) as RobbyTheRobot.RobbyTheRobot;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PopulationSizeNegativeConstructorTest()
        {
            RobbyTheRobot.RobbyTheRobot rob = Robby.CreateRobby(numberOfGenerations, -1789, 
            numberOfTrials, potentialSeed) as RobbyTheRobot.RobbyTheRobot;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void PopulationSizeZeroConstructorTest()
        {
            RobbyTheRobot.RobbyTheRobot rob = Robby.CreateRobby(numberOfGenerations, 0, 
            numberOfTrials, potentialSeed) as RobbyTheRobot.RobbyTheRobot;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NumberOfTrialsNegativeConstructorTest()
        {
            RobbyTheRobot.RobbyTheRobot rob = Robby.CreateRobby(numberOfGenerations, populationSize, 
            -12141, potentialSeed) as RobbyTheRobot.RobbyTheRobot;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NumberOfTrialsZeroConstructorTest()
        {
            RobbyTheRobot.RobbyTheRobot rob = Robby.CreateRobby(numberOfGenerations, populationSize, 
            0, potentialSeed) as RobbyTheRobot.RobbyTheRobot;
        }
    }
}