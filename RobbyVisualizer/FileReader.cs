using System;
using System.IO;
using GeneticAlgorithm;

namespace RobbyVisualizer{
    public class FileReader{

        // Properties
        public Chromosome Chromosome { get; private set;}
        public int Moves { get; private set;}
        public int? Generation { get; private set;}

        /// <summary>
        /// Constructor for the FileReader object.
        /// </summary>
        /// <param name="path"> The path to where the file is located </param>
        public FileReader(string path){
            GetData(ReadFile(path));
        }

        /// <summary>
        /// If the file doesnt exist, create and put dummy data, then reads from it
        /// Else read form the file
        /// </summary>
        /// <param name="path"> The path to where the file is located </param>
        /// <returns> String array that contains data from the file </returns>
        private string[] ReadFile(string path){
            path = @""+path;
            if(!File.Exists(path)){
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(path))
                {      
                    // Upload dummy data      
                    sw.WriteLine("null,234,200,064544666054216540412301440051023534141401364214536661035210461502045236560100422322203246553000056213626600063456404626656545355001120546220215300320312632405132106123553430634013462546526513115410624454354260436143633020364632111416561515133");
                }
            }
            using (StreamReader sr = File.OpenText(path))
            {
                string s;
                string[] data = new string[4];
                while ((s = sr.ReadLine()) != null)
                {
                    data = s.Split(",");
                }

                return data;
            }
        }

        /// <summary>
        /// Get the data from the file and stores it to the properties.
        /// </summary>
        /// <param name="data">The information from the file </param>
        private void GetData(string[] data){
            string geneString = data[3];
            int[] genes = new int[geneString.Length];

            for(int i = 0; i < genes.Length; i++){
                genes[i] = int.Parse(geneString[i].ToString());
            }
            Chromosome = new Chromosome(genes);

            Moves = int.Parse(data[2].ToString());
            if (data[0] == "null") Generation = null;
            else Generation = int.Parse(data[0].ToString());
        }
    }
}
