using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeneticAlgorithm;

namespace GeneticAlgorithmTest
{
    [TestClass]
    public class ChromosomeTest
    {
        private int seed = 1;
        private int length = 7;
        private int nbGenes = 243;

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ErrorNegativeConstructorTest()
        {
            Chromosome chromosomeTest = new Chromosome(-2,length,seed);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void ErrorZeroConstructorTest()
        {
            Chromosome chromosomeTest = new Chromosome(0,length,seed);
        }

        [TestMethod]
        public void ConstructorTest()
        {
            Chromosome chromosomeTest = new Chromosome(nbGenes,length,seed);

            int[] expectedGenes = {1, 0, 3, 5, 4, 3, 2, 6, 0, 4, 0, 1, 
            2, 6, 4, 4, 1, 4, 4, 4, 6, 0, 1, 2, 5, 1, 5, 2, 5, 6, 3, 5, 4, 0, 6, 5, 6, 0, 3, 3, 1, 6, 4, 2, 3, 4, 4, 3, 1, 2, 3, 1, 2, 
            4, 2, 1, 6, 0, 6, 3, 4, 0, 0, 1, 0, 2, 3, 2, 6, 6, 5, 1, 0, 1, 3, 6, 2, 5, 6, 0, 3, 2, 0, 3, 2, 2, 2, 0, 4, 5, 5, 5, 0, 0, 
            6, 1, 6, 3, 2, 0, 0, 2, 3, 2, 3, 2, 0, 1, 1, 3, 5, 1, 0, 5, 6, 2, 6, 4, 5, 4, 6, 3, 4, 1, 0, 0, 0, 0, 2, 1, 0, 6, 3, 5, 6, 0, 
            5, 4, 6, 0, 2, 1, 5, 1, 0, 4, 4, 1, 0, 3, 1, 1, 4, 4, 1, 4, 4, 2, 2, 3, 1, 6, 6, 0, 2, 5, 4, 2, 5, 6, 3, 2, 5, 5, 2, 5, 5, 3, 
            0, 2, 2, 6, 0, 5, 6, 6, 2, 6, 4, 2, 3, 3, 2, 2, 6, 2, 2, 4, 5, 1, 6, 2, 2, 1, 0, 6, 3, 5, 1, 3, 5, 1, 1, 0, 1, 1, 6, 1, 0, 3, 
            3, 5, 4, 3, 3, 6, 6, 2, 6, 0, 2, 0, 4, 5, 2, 0, 3, 6, 4, 0, 2, 3, 4};

            CollectionAssert.AreEqual(expectedGenes, chromosomeTest.Genes);
            Assert.AreEqual(7, chromosomeTest.Length);
        }

        [TestMethod]
        public void CopyConstructorTest()
        {
            Chromosome chromosomeTest = new Chromosome(nbGenes,length,seed);
            Chromosome copy = new Chromosome(chromosomeTest);

            int[] expectedGenes = {1, 0, 3, 5, 4, 3, 2, 6, 0, 4, 0, 1, 
            2, 6, 4, 4, 1, 4, 4, 4, 6, 0, 1, 2, 5, 1, 5, 2, 5, 6, 3, 5, 4, 0, 6, 5, 6, 0, 3, 3, 1, 6, 4, 2, 3, 4, 4, 3, 1, 2, 3, 1, 2, 
            4, 2, 1, 6, 0, 6, 3, 4, 0, 0, 1, 0, 2, 3, 2, 6, 6, 5, 1, 0, 1, 3, 6, 2, 5, 6, 0, 3, 2, 0, 3, 2, 2, 2, 0, 4, 5, 5, 5, 0, 0, 
            6, 1, 6, 3, 2, 0, 0, 2, 3, 2, 3, 2, 0, 1, 1, 3, 5, 1, 0, 5, 6, 2, 6, 4, 5, 4, 6, 3, 4, 1, 0, 0, 0, 0, 2, 1, 0, 6, 3, 5, 6, 0, 
            5, 4, 6, 0, 2, 1, 5, 1, 0, 4, 4, 1, 0, 3, 1, 1, 4, 4, 1, 4, 4, 2, 2, 3, 1, 6, 6, 0, 2, 5, 4, 2, 5, 6, 3, 2, 5, 5, 2, 5, 5, 3, 
            0, 2, 2, 6, 0, 5, 6, 6, 2, 6, 4, 2, 3, 3, 2, 2, 6, 2, 2, 4, 5, 1, 6, 2, 2, 1, 0, 6, 3, 5, 1, 3, 5, 1, 1, 0, 1, 1, 6, 1, 0, 3, 
            3, 5, 4, 3, 3, 6, 6, 2, 6, 0, 2, 0, 4, 5, 2, 0, 3, 6, 4, 0, 2, 3, 4};
            
            CollectionAssert.AreEqual(chromosomeTest.Genes, copy.Genes);
            CollectionAssert.AreEqual(expectedGenes, chromosomeTest.Genes);
            Assert.AreEqual(7, chromosomeTest.Length);
        }

        [TestMethod]
        public void ReproduceTest(){
            Chromosome parent1 = new Chromosome(nbGenes,length,seed);
            Chromosome parent2 = new Chromosome(nbGenes,length,2);

            int[] expectedChild1 = {1, 2, 1, 6, 4, 3, 2, 6, 0, 4, 0, 1, 2, 6, 4, 4, 1, 4, 4, 4, 6, 0, 1, 2, 5, 1, 5, 2, 
            5, 6, 3, 5, 4, 0, 6, 5, 6, 0, 3, 3, 1, 6, 4, 2, 3, 4, 4, 3, 1, 2, 3, 1, 2, 4, 2, 1, 6, 0, 6, 3, 4, 0, 0, 1, 
            0, 2, 3, 2, 6, 6, 5, 1, 0, 1, 3, 6, 2, 5, 6, 0, 3, 2, 0, 3, 2, 2, 2, 0, 4, 5, 5, 5, 0, 0, 6, 1, 6, 3, 2, 0, 
            0, 2, 3, 2, 3, 2, 0, 1, 1, 3, 5, 1, 0, 5, 6, 2, 6, 4, 5, 4, 6, 3, 4, 1, 0, 0, 0, 0, 2, 1, 0, 6, 3, 5, 6, 0, 
            5, 4, 6, 0, 2, 1, 5, 1, 0, 4, 4, 1, 0, 3, 1, 1, 4, 4, 1, 4, 4, 2, 2, 3, 1, 6, 6, 0, 2, 5, 4, 2, 5, 6, 3, 2, 
            5, 5, 2, 5, 5, 3, 0, 2, 2, 6, 0, 5, 6, 6, 2, 6, 4, 2, 3, 3, 2, 2, 6, 2, 2, 4, 5, 1, 6, 2, 2, 1, 0, 6, 3, 5, 
            1, 3, 5, 1, 1, 0, 1, 1, 6, 1, 0, 3, 3, 5, 4, 3, 3, 6, 6, 2, 6, 0, 2, 0, 4, 5, 2, 0, 3, 6, 4, 0, 2, 3, 4};

            int[] expectedChild2 = {5, 0, 3, 5, 0, 2, 5, 3, 1, 0, 5, 0, 0, 3, 2, 1, 4, 1, 5, 0, 0, 2, 2, 5, 6, 5, 4, 6, 
            0, 4, 6, 6, 0, 0, 2, 5, 4, 5, 4, 0, 0, 1, 1, 3, 6, 3, 0, 3, 2, 3, 4, 4, 0, 6, 4, 3, 0, 3, 0, 2, 4, 6, 2, 3, 
            0, 5, 6, 6, 1, 3, 4, 6, 3, 4, 0, 5, 0, 5, 5, 2, 5, 1, 4, 4, 0, 2, 6, 1, 3, 6, 5, 1, 4, 2, 3, 1, 6, 5, 2, 1, 
            4, 0, 2, 5, 6, 4, 0, 3, 5, 5, 2, 1, 4, 5, 3, 3, 2, 5, 3, 5, 6, 5, 3, 1, 5, 2, 2, 1, 1, 6, 6, 2, 2, 4, 4, 5, 
            5, 5, 5, 2, 1, 3, 2, 4, 3, 3, 4, 6, 6, 6, 6, 1, 1, 4, 2, 6, 3, 0, 0, 3, 2, 6, 1, 6, 6, 0, 4, 0, 0, 5, 4, 3, 
            0, 0, 3, 2, 2, 6, 4, 1, 5, 2, 1, 1, 0, 4, 0, 5, 2, 5, 1, 5, 5, 1, 6, 2, 1, 2, 5, 3, 4, 3, 6, 0, 2, 2, 1, 0, 
            0, 6, 3, 4, 2, 5, 5, 6, 4, 6, 6, 1, 6, 0, 5, 1, 3, 5, 5, 5, 0, 1, 1, 6, 0, 1, 3, 1, 3, 0, 6, 4, 3, 6, 4};

            IChromosome[] reproduceChilds = parent1.Reproduce(parent2, 0.1);            
            Chromosome child1 = reproduceChilds[0] as Chromosome;
            Chromosome child2 = reproduceChilds[1] as Chromosome;

            CollectionAssert.AreEqual(expectedChild1, child1.Genes);
            CollectionAssert.AreEqual(expectedChild2, child2.Genes);
        }

        [TestMethod]
        public void CompareToTest(){
            Chromosome chromosome = new Chromosome(nbGenes,length,null);
            Chromosome otherChromosome = new Chromosome(nbGenes,length, null);
            chromosome.Fitness = 1;
            otherChromosome.Fitness = 2;

            Assert.AreEqual(0, chromosome.CompareTo(chromosome));
            Assert.AreEqual(1, otherChromosome.CompareTo(chromosome));
            Assert.AreEqual(-1, chromosome.CompareTo(otherChromosome));
        }
    }
}