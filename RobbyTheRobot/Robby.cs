using System;

namespace RobbyTheRobot
{
    public static class Robby{
        /// <summary>
        /// Static method to instantiate IRobbytheRobot object with the input parameters
        /// </summary>
        /// <returns> IRobbyTheRobot object</returns>
        public static IRobbyTheRobot CreateRobby(int numberOfGenerations, int populationSize, int numberOfTrials, int? potentialSeed){
            return new RobbyTheRobot(numberOfGenerations, populationSize, numberOfTrials, potentialSeed);
        }
    }
}
