using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GeneticAlgorithm;

namespace GeneticAlgorithmTest
{   
    [TestClass]
    public class GenerationTest{

        int populationSize = 200;
        int numberOfGenes = 243;
        int lengthOfGene = 7;
        double mutationRate = 1;
        double eliteRate = 1;
        int numberOfTrials = 1;
        FitnessEventHandler FitnessEvent;
        int potentialSeed = 1;

        private double FitnessFunction(IChromosome chromosome, IGeneration generation){
            return 100;
        }
        
        [TestMethod]
        public void SortPopulationTest()
        {
            Random rand = new Random();

            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  eliteRate, numberOfTrials, FitnessEvent += (Chromosome, generation) => { return 100;}, //return (double) rand.Next(1000);}, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;

            Generation gen = geneticAlgorithm.CurrentGeneration as Generation;

            gen.EvaluateFitnessOfPopulation();
            Assert.AreEqual(100, gen.Population[0].Fitness);
            Assert.AreEqual(100, gen.MaxFitness);
        } 


        [TestMethod]
        public void FitnessPropertiesTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  eliteRate, numberOfTrials, FitnessEvent += (Chromosome, generation) => {return 0.5;}, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;

            Generation gen = geneticAlgorithm.CurrentGeneration as Generation;
            gen.EvaluateFitnessOfPopulation();
            gen.SortPopulation();

            Assert.AreEqual(0.5, gen.MaxFitness);
            Assert.AreEqual(0.5, gen.AverageFitness);
        } 

        [TestMethod]
        public void ConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  eliteRate, numberOfTrials, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;

            Generation gen = new Generation(geneticAlgorithm, FitnessEvent, potentialSeed);

            int[] expectedGenes = {1, 0, 3, 5, 4, 3, 2, 6, 0, 4, 0, 1, 2, 6, 4, 4, 1, 4, 4, 4, 6, 0, 1, 
            2, 5, 1, 5, 2, 5, 6, 3, 5, 4, 0, 6, 5, 6, 0, 3, 3, 1, 6, 4, 2, 3, 4, 4, 3, 1, 2, 3, 1, 2, 4, 
            2, 1, 6, 0, 6, 3, 4, 0, 0, 1, 0, 2, 3, 2, 6, 6, 5, 1, 0, 1, 3, 6, 2, 5, 6, 0, 3, 2, 0, 3, 2, 
            2, 2, 0, 4, 5, 5, 5, 0, 0, 6, 1, 6, 3, 2, 0, 0, 2, 3, 2, 3, 2, 0, 1, 1, 3, 5, 1, 0, 5, 6, 2, 
            6, 4, 5, 4, 6, 3, 4, 1, 0, 0, 0, 0, 2, 1, 0, 6, 3, 5, 6, 0, 5, 4, 6, 0, 2, 1, 5, 1, 0, 4, 4, 
            1, 0, 3, 1, 1, 4, 4, 1, 4, 4, 2, 2, 3, 1, 6, 6, 0, 2, 5, 4, 2, 5, 6, 3, 2, 5, 5, 2, 5, 5, 3, 
            0, 2, 2, 6, 0, 5, 6, 6, 2, 6, 4, 2, 3, 3, 2, 2, 6, 2, 2, 4, 5, 1, 6, 2, 2, 1, 0, 6, 3, 5, 1, 
            3, 5, 1, 1, 0, 1, 1, 6, 1, 0, 3, 3, 5, 4, 3, 3, 6, 6, 2, 6, 0, 2, 0, 4, 5, 2, 0, 3, 6, 4, 0, 2, 3, 4};

            Assert.AreEqual(200, gen.NumberOfChromosomes);
            CollectionAssert.AreEqual(expectedGenes, gen.Population[3].Genes);
        }

        [TestMethod]    
        public void CopyConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  eliteRate, numberOfTrials, FitnessEvent += FitnessFunction, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;

            Generation gen = geneticAlgorithm.CurrentGeneration as Generation;

            Generation copyGen = new Generation(gen);
            Assert.AreEqual(2, Generation.CurrentGenerationNumber);
            Assert.AreEqual(gen.NumberOfChromosomes, copyGen.NumberOfChromosomes);
        }

        [TestMethod]
        [ExpectedException(typeof(NullReferenceException))]
        public void EmptyFitnessConstructorTest()
        {
            GeneticAlgorithm.GeneticAlgorithm geneticAlgorithm = GeneticLib.CreateGeneticAlgorithm(populationSize, 
            numberOfGenes, lengthOfGene, 
            mutationRate,  eliteRate, numberOfTrials, FitnessEvent, 
            potentialSeed) as GeneticAlgorithm.GeneticAlgorithm;
            Generation gen = new Generation(geneticAlgorithm, FitnessEvent, potentialSeed);
        }
    }
}