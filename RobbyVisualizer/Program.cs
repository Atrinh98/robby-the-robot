﻿using System;
using RobbyTheRobot;

namespace RobbyVisualizer
{
    public static class Program
    {
        [STAThread]
        /// <summary>
        /// The mmonogame application to run the simulation
        /// </summary>
        static void Main()
        {
            RobbyTheRobot.RobbyTheRobot robby = Robby.CreateRobby(1, 200, 1, null) as RobbyTheRobot.RobbyTheRobot;  
            using (var game = new RobbyVisualizerGame(robby))
                game.Run();
        }
    }
}
