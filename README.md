# Robby the Robot

## Demo

https://www.youtube.com/watch?v=kFa-_nmTHcY

## Description

A chromosome is like a code that Robby follows to pick up cans.

Robby is a machine learning program. With generations of Robby learning how to pick up cans. 
The latest generation will have Robby's strongest chromosome demonstrating a good skill of picking up cans.
The program will randomly generate 50 cans on a 10 by 10 board. Robby will use a chromosome chosen from a generation to be picked 
between (1, 20, 100, 200, 500, 1000). When selecting a generation, Robby will start moving and collecting cans with
that chromosome's generation.

## Technology Stack

- C#
- MonoGame
- GIMP
- C# Unit Test

## How to Run

Open Robby-the-Robot project folder in your preferred IDE

If you want to generate your own data (go to Step 1)
Else go to step (Step 3) to just play Robby picking up cans

1. Go to the project folder .\RobbyIterationGenerator\
2. Run the command dotnet run in your terminal
3. Go to the project folder .\RobbyVisualizer\
4. Run the command dotnet run in your terminal

## Usage

When running RobbyIterationGenerator
1. The console application will appear and you will be prompted to answer questions relating to Robby's generations
    - You can press enter to have default values or put your own
    - For the data to be more accurate use at least the minimum for the data when asked
2. Wait for the program to finish gathering data
    - New data will get appended to each specific txt file

When running RobbyVisualizer
1. The application will appear and will prompt you to select a number
    - Number selected will be the code that Robby will execute to pick up cans
2. When Robby has exausted all he's moves
    - You can select a new number to make Robby collect cans again
    - Press esc key on your keyboard or exit the window to close application