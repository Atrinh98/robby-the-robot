﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RobbyTheRobot;
using GeneticAlgorithm;

namespace RobbyVisualizer
{
    public class RobbyVisualizerGame : Game
    {

        // Fields
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private SpriteFont _arialFont;
        private RobbyTheRobot.RobbyTheRobot _robby;
        private ContentsOfGrid[,] _grid;
        private SimulationSprite _simulationSprite;

        // Fields for time
        private float _timer;
        private float _delay;

        // Information Properties
        private double _points;
        private bool _done;
        public static Chromosome Chromosome {get; set;}

        public static int? Generation {get; set;}

        public static int Moves {get; set;}

        public static int MovesLeft {get; set;}

        public static int Cans {get; set;}

        // Fields about robby coordinates
        private static  int _x;

        private static int _y;

        /// <summary>
        /// Getter for x value where robby is (can not put properties as ref)
        /// </summary>
        /// <returns> x value of where robby is </returns>
        public static int GetX(){
            return _x;
        }

        /// <summary>
        /// Getter for y value where robby is (can not put properties as ref)
        /// </summary>
        /// <returns> y value of where robby is </returns>
        public static int GetY(){
            return _y;
        }

        /// <summary>
        /// Constructor for the RobbySprite object.
        /// </summary>
        /// <param name="game"> The base game </param>
        public RobbyVisualizerGame(RobbyTheRobot.RobbyTheRobot robby)
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            _robby = robby;

            _done = false;
            _delay = 250;
            _timer = _delay;
            _grid = _robby.GenerateRandomTestGrid();
            int[] coordinates = _robby.PlaceRobby(_grid);
            _x = coordinates[0];
            _y = coordinates[1];
        }

        /// <summary>
        /// Initialize function (default)
        /// </summary>
        protected override void Initialize()
        {
            _graphics.PreferredBackBufferWidth = 320;
            _graphics.PreferredBackBufferHeight = 400;
            _graphics.ApplyChanges();
 
            ButtonSprite buttonSprite = new ButtonSprite(this);
            buttonSprite.DrawOrder = 3;
            Components.Add(buttonSprite); 
            _simulationSprite = new SimulationSprite(this, _robby.GridSize, _grid);
            _simulationSprite.DrawOrder = 1;
            Components.Add(_simulationSprite);
            RobbySprite rob = new RobbySprite(this);
            rob.DrawOrder = 2;
            Components.Add(rob);

            base.Initialize();
        }

        /// <summary>
        /// Load all the sprites needed for the game
        /// </summary>
        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _arialFont = Content.Load<SpriteFont>("Arial");
        }

        /// <summary>
        /// Timer to for when robby moves
        /// </summary>
        /// <param name="gametime"> </param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // To reset the board
            if (MovesLeft == 0){
                _done = true;
                ButtonSprite.Click = false;
            }
            if (_done == true && MovesLeft != 0){
                _done = false;
                _grid = _robby.GenerateRandomTestGrid();
                int[] coordinates = _robby.PlaceRobby(_grid);
                _x = coordinates[0];
                _y = coordinates[1];
                _simulationSprite.UpdateGrid(_grid);
                _points = 0;
            }

            // Timer
            _timer -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;

            if (_timer <= 0 && Generation != 0 && MovesLeft != 0){
                _points += _robby.MoveRobbyVisualizer(Chromosome, _grid, ref _x, ref _y);
                MovesLeft--;
                // Reset timer
               _timer = _delay;
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// Shows the information about the game in current time
        /// </summary>
        /// <param name="gametime"> </param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.DimGray);
 
            _spriteBatch.Begin();
            // Base information to display
            string generation;
            if (Generation == null) generation = "Current Generation: N/A";
            else generation = "Current Generation: " + Generation;
            _spriteBatch.DrawString(_arialFont, generation, new Vector2(0, 330), Color.White);
            string move = "Moves: " + MovesLeft + "/" + Moves;
            _spriteBatch.DrawString(_arialFont, move, new Vector2(0, 355), Color.White);
            string point = "Points: " + _points;
            _spriteBatch.DrawString(_arialFont, point, new Vector2(0, 380), Color.White);

            // Extra information
            string cans = "Cans Left: " + Cans;
            _spriteBatch.DrawString(_arialFont, cans, new Vector2(220, 330), Color.White);

            string robX = "X: " + _x;
            _spriteBatch.DrawString(_arialFont, robX, new Vector2(280, 355), Color.White);

            string robY = "Y: " + _y;
            _spriteBatch.DrawString(_arialFont, robY, new Vector2(280, 380), Color.White);
            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
