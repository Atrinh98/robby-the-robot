using System;
using System.Diagnostics;

namespace GeneticAlgorithm{
    public class GeneticAlgorithm : IGeneticAlgorithm{

        // Fields
        private int? _potentialSeed;

        /// <summary>
        /// Constructor for GeneticAlgoirthm where we store the information for generation and generates a first generation object.
        /// </summary>
        public GeneticAlgorithm(int populationSize, int numberOfGenes, int lengthOfGene, 
        double mutationRate, double eliteRate, int numberOfTrials, FitnessEventHandler fitnessFunction, 
        int? potentialSeed){

            if (populationSize <= 0 || numberOfGenes <= 0 || numberOfTrials <= 0 
            || eliteRate <= 0 || mutationRate <=0) 
            throw new ArgumentException("Population size, Number of genes, Elite Rate, Mutation Rate, or Number of Trials cannot be under 0 or 0.");

            Debug.Assert(fitnessFunction.GetInvocationList().Length > 0);

            PopulationSize = populationSize;
            NumberOfGenes = numberOfGenes;
            LengthOfGene = lengthOfGene;
            MutationRate = mutationRate;
            EliteRate = eliteRate;
            NumberOfTrials = numberOfTrials;
            FitnessCalculation += fitnessFunction;

            if (potentialSeed != null) _potentialSeed = (int) potentialSeed;
            else _potentialSeed =  potentialSeed;

            Generation.CurrentGenerationNumber = 0;
            CurrentGeneration = GenerateGeneration() as Generation; 
        }

        // Properties
        public int PopulationSize { get; }
        public int NumberOfGenes { get; }
        public int LengthOfGene { get; }
        public double MutationRate { get; }
        public double EliteRate { get; }

        /// <summary>
        /// The number of times the fitness function should be called when computing the result
        /// </summary>
        public int NumberOfTrials {get;}

        /// <summary>
        /// The current number of generations generated since the start of the algorithm
        /// </summary>
        public long GenerationCount {get { return Generation.CurrentGenerationNumber; } }

        /// <summary>
        /// Returns the current generation
        /// </summary>
        public IGeneration CurrentGeneration { get; private set; } 

        /// <summary>
        /// The delegate of the fitness method to be called
        /// </summary>
        /// <value></value>
        public FitnessEventHandler FitnessCalculation {get;}


        //---------------------------------CHECK THIS-------------------------//

        /// <summary>
        /// Generates a generation for the given parameters. If no generation has been created the initial one will be constructed. 
        /// If a generation has already been created, it will provide the next generation.
        /// </summary>
        /// <returns>The current generation</returns>
        public IGeneration GenerateGeneration(){
            // If no generation has been constructed then used this.
            if (GenerationCount == 0){
                CurrentGeneration = new Generation(this, FitnessCalculation, _potentialSeed);
                return CurrentGeneration;
            }
            // Else created next generation.
            else{
                return GenerateNextGeneration();
            }
        }

        /// <summary>
        /// Generates a generation with the current info and update the generation number + population
        /// </summary>
        /// <returns>The next generation</returns>
        private IGeneration GenerateNextGeneration(){
            CurrentGeneration = new Generation(CurrentGeneration as Generation); 
            return CurrentGeneration;
        }
    }   
}